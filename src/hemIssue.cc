#include "Corrections/JME/interface/hemIssue.h"




std::pair<std::vector<unsigned int>, ROOT::RVec<bool>> listJetsInHEMRegion(ROOT::VecOps::RVec<float> const& Jet_pt, ROOT::VecOps::RVec<float> const& Jet_eta,  ROOT::VecOps::RVec<float> const& Jet_phi, 
        ROOT::VecOps::RVec<Char_t> const& Jet_jetId, ROOT::VecOps::RVec<Char_t> const& Jet_puId, ROOT::VecOps::RVec<float> const& Jet_chEmEF, ROOT::VecOps::RVec<float> const& Jet_neEmEF, 
        ROOT::VecOps::RVec<Short_t> const& Jet_muonIdx1, ROOT::VecOps::RVec<Short_t> const& Jet_muonIdx2) {
    std::vector<unsigned int> jets_in_veto_region;
    ROOT::RVec<bool> jets_passing_hem_preselections(Jet_pt.size(), false);
    for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
        if (Jet_pt[ijet] <= 15) continue;
        if (!(
            Jet_jetId[ijet] >= 6 // tight jetId with lepVeto
            || // tight jet ID & (jet EM fraction < 0.9) & (jets that don’t overlap with PF muon (dR < 0.2)
            (Jet_jetId[ijet] >= 2  && Jet_chEmEF[ijet]+Jet_neEmEF[ijet] <0.9 && Jet_muonIdx1[ijet] < 0 && Jet_muonIdx2[ijet] < 0))
            ) 
            continue; 
        if (Jet_puId[ijet] < 1 && Jet_pt[ijet] <= 50)
            continue;
        
        jets_passing_hem_preselections[ijet] = true;
        if (Jet_eta[ijet]> -3.2 && Jet_eta[ijet] < -1.3 && Jet_phi[ijet] > -1.57 && Jet_phi[ijet] < 0.87) {
            jets_in_veto_region.push_back(ijet);
        }  
    }
    return {jets_in_veto_region, jets_passing_hem_preselections};
}


std::pair<std::vector<unsigned int>, ROOT::RVec<bool>> listJetsInHEMRegion_hleprare_skim(ROOT::VecOps::RVec<float> const& Jet_pt, ROOT::VecOps::RVec<float> const& Jet_eta,  ROOT::VecOps::RVec<float> const& Jet_phi, 
        ROOT::VecOps::RVec<Char_t> const& Jet_jetId, ROOT::VecOps::RVec<Char_t> const& Jet_puId) {
    std::vector<unsigned int> jets_in_veto_region;
    ROOT::RVec<bool> jets_passing_hem_preselections(Jet_pt.size(), false);
    for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
        if (Jet_pt[ijet] <= 15) continue;
        if (!(
            Jet_jetId[ijet] >= 6 // tight jetId with lepVeto
            //|| // tight jet ID & (jet EM fraction < 0.9) & (jets that don’t overlap with PF muon (dR < 0.2)
            //(Jet_jetId[ijet] >= 2  && Jet_chEmEF[ijet]+Jet_neEmEF[ijet] <0.9 && Jet_muonIdx1[ijet] < 0 && Jet_muonIdx2[ijet] < 0)
            )
            ) 
            continue; 
        if (Jet_puId[ijet] < 1 && Jet_pt[ijet] <= 50)
            continue;
        
        jets_passing_hem_preselections[ijet] = true;
        if (Jet_eta[ijet]> -3.2 && Jet_eta[ijet] < -1.3 && Jet_phi[ijet] > -1.57 && Jet_phi[ijet] < -0.87) {
            jets_in_veto_region.push_back(ijet);
        }  
    }
    return {jets_in_veto_region, jets_passing_hem_preselections};
}

