#include "Corrections/JME/interface/metShift.h"

// Constructors
metShift::metShift () {}

// Destructor
metShift::~metShift() {}

std::vector<double> metShift::get_shifted_met(
    ROOT::VecOps::RVec<float> const& object_eta,
    ROOT::VecOps::RVec<float> const& object_phi,
    ROOT::VecOps::RVec<float> const& object_initial_pt,
    ROOT::VecOps::RVec<float> const& object_initial_mass,
    ROOT::VecOps::RVec<float> const& object_final_pt,
    ROOT::VecOps::RVec<float> const& object_final_mass,
    ROOT::VecOps::RVec<bool> const& shouldCorrectObject,
    float Met_pt,
    float Met_phi
  )
{
  auto met_tlv = TLorentzVector();
  met_tlv.SetPxPyPzE(Met_pt * cos(Met_phi), Met_pt * sin(Met_phi), 0, Met_pt);
  for (size_t iJet = 0; iJet < object_initial_pt.size(); iJet++) {
    if (!shouldCorrectObject[iJet]) continue;
    auto object_initial_tlv = TLorentzVector();
    object_initial_tlv.SetPtEtaPhiM(object_initial_pt[iJet], object_eta[iJet], object_phi[iJet], object_initial_mass[iJet]);
    auto object_final_tlv = TLorentzVector();
    object_final_tlv.SetPtEtaPhiM(object_final_pt[iJet], object_eta[iJet], object_phi[iJet], object_final_mass[iJet]);
    met_tlv += object_initial_tlv;
    met_tlv -= object_final_tlv;
  }
  return {met_tlv.Pt(), met_tlv.Phi()};
};


std::vector<double> metShift::get_shifted_met_indices(
    ROOT::VecOps::RVec<float> const& object_eta,
    ROOT::VecOps::RVec<float> const& object_phi,
    ROOT::VecOps::RVec<float> const& object_initial_pt,
    ROOT::VecOps::RVec<float> const& object_initial_mass,
    ROOT::VecOps::RVec<float> const& object_final_pt,
    ROOT::VecOps::RVec<float> const& object_final_mass,
    ROOT::VecOps::RVec<int> const& objectsToCorrect, // List of indices in object collection to correct for
    float Met_pt,
    float Met_phi
  )
{
  auto met_tlv = TLorentzVector();
  met_tlv.SetPxPyPzE(Met_pt * cos(Met_phi), Met_pt * sin(Met_phi), 0, Met_pt);
  for (int iJet : objectsToCorrect) {
    if (iJet < 0 || iJet >= (int)object_eta.size())
      throw std::runtime_error("Invalid object index when trying to propagate variation to MET");
    auto object_initial_tlv = TLorentzVector();
    object_initial_tlv.SetPtEtaPhiM(object_initial_pt[iJet], object_eta[iJet], object_phi[iJet], object_initial_mass[iJet]);
    auto object_final_tlv = TLorentzVector();
    object_final_tlv.SetPtEtaPhiM(object_final_pt[iJet], object_eta[iJet], object_phi[iJet], object_final_mass[iJet]);
    met_tlv += object_initial_tlv;
    met_tlv -= object_final_tlv;
  }
  return {met_tlv.Pt(), met_tlv.Phi()};
};
