#include "Corrections/JME/interface/jecProvider.h"
#include <iostream>


// Constructors
jecProvider::jecProvider (std::string jerInputFileName, std::vector<std::string> jec_sources)
{
  for (const auto& source: jec_sources) {
    JetCorrectorParameters source_parameters_reduced(jerInputFileName, source);
    std::unique_ptr<JetCorrectionUncertainty> source_uncertainty_reduced(
      new JetCorrectionUncertainty(source_parameters_reduced));
    jecSourceUncRegroupedProviders_.emplace(source, std::move(source_uncertainty_reduced));
  }
  jec_sources_regrouped_ = jec_sources;
}

// Destructor
jecProvider::~jecProvider() {}

std::vector<std::vector<std::vector<float>>> jecProvider::get_jec_uncertainties(
    size_t nJet,
    const ROOT::VecOps::RVec<float>& Jet_pt,
    const ROOT::VecOps::RVec<float>& Jet_eta
)
{
  std::vector <std::vector<float>> jec_uncertainties_up;
  std::vector <std::vector<float>> jec_uncertainties_down;

  for (auto source: jec_sources_regrouped_) {
    std::vector<float> unc_up(nJet, -999.);
    std::vector<float> unc_down(nJet, -999.);

    for (size_t ijet = 0; ijet < nJet; ijet++) {
      jecSourceUncRegroupedProviders_[source]->setJetEta(Jet_eta[ijet]);
      jecSourceUncRegroupedProviders_[source]->setJetPt(Jet_pt[ijet]);
      float jecResult = jecSourceUncRegroupedProviders_[source]->getUncertainty(true);
      if (jecResult < -900) { // Avoid propagating nonsensical values. This happens in case pt or eta out of range (either because -1 was given as input, or in very rare cases due to bugs in input dataset) 
        jecResult = 0;
        std::cerr << "WARNING : Corrections/JME/jecProvider.cc : JEC jet input out of range, pt=" << Jet_pt[ijet] << " eta=" << Jet_eta[ijet] << std::endl;
      }
      unc_up[ijet] = 1 + jecResult;

      jecSourceUncRegroupedProviders_[source]->setJetEta(Jet_eta[ijet]);
      jecSourceUncRegroupedProviders_[source]->setJetPt(Jet_pt[ijet]);
      jecResult = jecSourceUncRegroupedProviders_[source]->getUncertainty(false);
      if (jecResult < -900) {
        jecResult = 0;
        std::cerr << "WARNING : Corrections/JME/jecProvider.cc : JEC jet input out of range, pt=" << Jet_pt[ijet] << " eta=" << Jet_eta[ijet] << std::endl;
      }
      unc_down[ijet] = 1 - jecResult;
    }
    jec_uncertainties_up.push_back(unc_up);
    jec_uncertainties_down.push_back(unc_down);
  }
  return {jec_uncertainties_up, jec_uncertainties_down};
};

std::vector<std::vector<float>> jecProvider::get_single_jec_uncertainties(
    float Jet_pt,
    float Jet_eta
)
{
  std::vector <float> jec_uncertainties_up;
  std::vector <float> jec_uncertainties_down;

  for (auto source: jec_sources_regrouped_) {
    jecSourceUncRegroupedProviders_[source]->setJetEta(Jet_eta);
    jecSourceUncRegroupedProviders_[source]->setJetPt(Jet_pt);
    jec_uncertainties_up.push_back(
      1 + jecSourceUncRegroupedProviders_[source]->getUncertainty(true));

    jecSourceUncRegroupedProviders_[source]->setJetEta(Jet_eta);
    jecSourceUncRegroupedProviders_[source]->setJetPt(Jet_pt);
    jec_uncertainties_down.push_back(
      1 - jecSourceUncRegroupedProviders_[source]->getUncertainty(false));
  }
  return {jec_uncertainties_up, jec_uncertainties_down};
};
