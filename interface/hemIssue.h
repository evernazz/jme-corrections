#ifndef hemIssue_h
#define hemIssue_h

// Standard libraries

#include <ROOT/RVec.hxx>




///< Returns pair(list of jet indices falling in HEM veto region; Jet_sized bool array of jets passing preselection for HEM (not necessarily in eta/phi HEM issue region))
std::pair<std::vector<unsigned int>, ROOT::RVec<bool>> listJetsInHEMRegion(ROOT::VecOps::RVec<float> const& Jet_pt, ROOT::VecOps::RVec<float> const& Jet_eta, ROOT::VecOps::RVec<float> const& Jet_phi, 
    ROOT::VecOps::RVec<Char_t> const& Jet_jetId, ROOT::VecOps::RVec<Char_t> const& Jet_puId, ROOT::VecOps::RVec<float> const& Jet_chEmEF, ROOT::VecOps::RVec<float> const& Jet_neEmEF, 
    ROOT::VecOps::RVec<Short_t> const& Jet_muonIdx1, ROOT::VecOps::RVec<Short_t> const& Jet_muonIdx2);

///< Version suited for HLepRare skims that don't have the jet EM fraction and muonIdx branches. For temporary use only !
std::pair<std::vector<unsigned int>, ROOT::RVec<bool>> listJetsInHEMRegion_hleprare_skim(ROOT::VecOps::RVec<float> const& Jet_pt, ROOT::VecOps::RVec<float> const& Jet_eta,  ROOT::VecOps::RVec<float> const& Jet_phi, 
        ROOT::VecOps::RVec<Char_t> const& Jet_jetId, ROOT::VecOps::RVec<Char_t> const& Jet_puId);


#endif // hemIssue_h
