#ifndef metShift_h
#define metShift_h

// Standard libraries

#include <TLorentzVector.h>
#include <TMath.h>
#include <ROOT/RVec.hxx>

// metShift class
class metShift {
  public:
    metShift ();
    ~metShift ();
    std::vector<double> get_shifted_met(
      ROOT::VecOps::RVec<float> const& object_eta,
      ROOT::VecOps::RVec<float> const& object_phi,
      ROOT::VecOps::RVec<float> const& object_initial_pt,
      ROOT::VecOps::RVec<float> const& object_initial_mass,
      ROOT::VecOps::RVec<float> const& object_final_pt,
      ROOT::VecOps::RVec<float> const& object_final_mass,
      ROOT::VecOps::RVec<bool> const& shouldCorrectObject, // same length as object_*, if false do not correct this object (because for ex it does not pass selections of jets to be propagated to MET, ie muon-matched)
      float Met_pt,
      float Met_phi
    );
    // Version with a list of indices of objects to correct
    std::vector<double> get_shifted_met_indices(
      ROOT::VecOps::RVec<float> const& object_eta,
      ROOT::VecOps::RVec<float> const& object_phi,
      ROOT::VecOps::RVec<float> const& object_initial_pt,
      ROOT::VecOps::RVec<float> const& object_initial_mass,
      ROOT::VecOps::RVec<float> const& object_final_pt,
      ROOT::VecOps::RVec<float> const& object_final_mass,
      ROOT::VecOps::RVec<int> const& objectsToCorrect, // List of indices in object collection to correct for
      float Met_pt,
      float Met_phi
  );

};

#endif // metShift_h
