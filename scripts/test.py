import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.JME.jetCorrections import jecRDF
from Corrections.JME.smearing import jetSmearerRDF, jetVarRDF
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"

def jec_test(df, isMC, year, runPeriod, runEra, jectype):
    jec = jecRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod,
        runEra=runEra,
        type=jectype
    )()
    df, _ = jec.run(df)
    var = "jec_sf"
    if jectype == 'fatjerc': var = "fatjec_sf" 
    h = df.Histo1D(var)
    print(f"Jet {year}{runPeriod} Era{runEra} {var} Integral: %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def jer_test(df, isMC, year, runPeriod, runEra, jectype, after_jec):
    jer = jetSmearerRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod,
        runEra=runEra,
        type=jectype,
        after_jec=after_jec
    )()
    df, _ = jer.run(df)
    jervar = jetVarRDF(
        isMC=isMC,
        type=jectype,
        after_jec=after_jec
    )()
    df, _ = jervar.run(df)
    var = "jet_smear_factor"
    if jectype == 'fatjerc': var = "fatjet_smear_factor" 
    h = df.Histo1D(var)
    print(f"Jet {year}{runPeriod} Era{runEra} {var} (after_jec={after_jec}) %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))

    return df


if __name__ == "__main__":
    df_mc2022 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022.root")))
    _ = jec_test(df_mc2022, True, 2022, "preEE", "_", "jerc")
    _ = jer_test(_, True, 2022, "preEE", "_", "jerc", True)
    _ = jer_test(df_mc2022, True, 2022, "preEE", "_", "jerc", False)
    _ = jec_test(df_mc2022, True, 2022, "preEE", "_", "fatjerc")
    _ = jer_test(_, True, 2022, "preEE", "_", "fatjerc", True)
    _ = jer_test(df_mc2022, True, 2022, "preEE", "_", "fatjerc", False)

    df_mc2022_postEE = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022_postee.root")))
    _ = jec_test(df_mc2022_postEE, True, 2022, "postEE", "_", "jerc")
    _ = jer_test(_, True, 2022, "postEE", "_", "jerc", True)
    _ = jer_test(df_mc2022_postEE, True, 2022, "postEE", "_", "jerc", False)
    _ = jec_test(df_mc2022_postEE, True, 2022, "postEE", "_", "fatjerc")
    _ = jer_test(_, True, 2022, "postEE", "_", "fatjerc", True)
    _ = jer_test(df_mc2022_postEE, True, 2022, "postEE", "_", "fatjerc", False)

    df_data2022 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_data2022.root")))
    _ = jec_test(df_data2022, False, 2022, "preEE", "C", "jerc")
    _ = jec_test(df_data2022, False, 2022, "preEE", "C", "fatjerc")

    df_data2022_postEE = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_data2022_postee.root")))
    _ = jec_test(df_data2022_postEE, False, 2022, "postEE", "E", "jerc")
    _ = jec_test(df_data2022_postEE, False, 2022, "postEE", "E", "fatjerc")

    df_mc2023 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2023.root")))
    _ = jec_test(df_mc2023, True, 2023, "preBPix", "_", "jerc")
    _ = jer_test(_, True, 2023, "preBPix", "_", "jerc", True)
    _ = jer_test(df_mc2023, True, 2023, "preBPix", "_", "jerc", False)
    _ = jec_test(df_mc2023, True, 2023, "preBPix", "_", "fatjerc")
    _ = jer_test(_, True, 2023, "preBPix", "_", "fatjerc", True)
    _ = jer_test(df_mc2023, True, 2023, "preBPix", "_", "fatjerc", False)

    df_mc2023_postBpix = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2023_postbpix.root")))
    _ = jec_test(df_mc2023_postBpix, True, 2023, "postBPix", "_", "jerc")
    _ = jer_test(_, True, 2023, "postBPix", "_", "jerc", True)
    _ = jer_test(df_mc2023_postBpix, True, 2023, "postBPix", "_", "jerc", False)
    _ = jec_test(df_mc2023_postBpix, True, 2023, "postBPix", "_", "fatjerc")
    _ = jer_test(_, True, 2023, "postBPix", "_", "fatjerc", True)
    _ = jer_test(df_mc2023_postBpix, True, 2023, "postBPix", "_", "fatjerc", False)

    df_data2023 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_data2023.root")))
    _ = jec_test(df_data2023, False, 2023, "preBPix", "Cv2", "jerc")
    _ = jec_test(df_data2023, False, 2023, "preBPix", "Cv2", "fatjerc")

    df_data2023_postBpix = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_data2023_postbpix.root")))
    _ = jec_test(df_data2023_postBpix, False, 2023, "postBPix", "D", "jerc")
    _ = jec_test(df_data2023_postBpix, False, 2023, "postBPix", "D", "fatjerc")



