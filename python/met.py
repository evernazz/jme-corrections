""" All things MET related : propagation of corrections to MET & MET XY shift corrections """
import os

from Base.Modules.baseModules import JetLepMetSyst

from analysis_tools.utils import import_root, randomize

ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

json_path = "/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/POG/JME/{}/met.json.gz"

class metShifterRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.after_jec = kwargs.pop("after_jec", "")
        self.propagate_jer_smearing = kwargs.pop("propagate_jer_smearing", True)
        self.propagate_tes = kwargs.pop("propagate_tes")
        self.propagate_electron = kwargs.pop("propagate_electron")

        if self.isMC:
            if not os.getenv("_metShifter"):
                os.environ["_metShifter"] = "_metShifter"
                if "/libCorrectionsJME.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gSystem.Load("libCorrectionsJME.so")
                base = "{}/{}/src/Corrections/JME".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
                if not os.getenv("_METShift"):
                    os.environ["_METShift"] = "METShift"
                    ROOT.gROOT.ProcessLine(".L {}/interface/metShift.h".format(base))
                ROOT.gInterpreter.Declare('auto met_shifter = metShift();')


    def run(self, df):
        if not self.isMC:
            return df, []

        if self.after_jec:
            input_jet_central = "_corr" # not sure this is correct. In case we reapply the JEC (and undo the nominal JEC of the Nano), we need to undo the JEC in the MET as well I think. Thus not using _corr
        else:
            input_jet_central = ""
        
        applied_met_shift = False
        previous_met_pt = "MET_pt"
        previous_met_phi = "MET_phi"
        if (self.jet_syst != "" or input_jet_central != "") and (self.propagate_jer_smearing or not ("smeared_up" in self.jet_syst or "smeared_down" in self.jet_syst)): # diasble smearing of MET
            # Propagate JER or JEC to MET
            # normally we don't propagate JER to MET
            # jet_syst can be :
            # 1) nominal everything : _nom(=jet_nominal)  
            # 2) JEC syst variation : _smeared_FlavorQCD_up 
            # 3) JER syst variation : _smeared_up 
            if self.propagate_jer_smearing:
                considered_syst = self.jet_syst
            else:
                # disable propagation of JER to MET
                if self.after_jec: # in this case we want to propagate the JEC (incl nominal) to MET but not the JER
                    considered_syst = self.jet_syst.replace("_smeared_up", "", 1)
                    considered_syst = self.jet_syst.replace("_smeared_down", "", 1)
                else:
                    assert (not ("smeared_up" in self.jet_syst or "smeared_down" in self.jet_syst)) # we should not be here in this case, in case of JER variation we don't propagate any jet thing to MET 
                considered_syst = self.jet_syst.replace("_smeared", "", 1) # in case we are in JER nominal
                # now considered_syst should be 

            print(f"MET shift : propagating jet corrections to MET, from Jet_pt{input_jet_central} to Jet_pt{considered_syst}")
            branch = randomize(f"smeared_met_jet_{input_jet_central}_{self.jet_syst}")
            df = df.Define(branch, "met_shifter.get_shifted_met("
                            f"Jet_eta, Jet_phi, Jet_pt{input_jet_central}, Jet_mass{input_jet_central}, Jet_pt{considered_syst}, Jet_mass{considered_syst}, "
                            "(Jet_pt>15 && Jet_muonIdx1<0 && Jet_muonIdx2<0 && (Jet_chEmEF+Jet_neEmEF)<0.9)," # selection on jets to be propagated to MET (https://cms-talk.web.cern.ch/t/jetmet-corrections-for-wprime-l-nu-run-3-analysis/43063/2?u=tcuisset)
                            f"{previous_met_pt}, {previous_met_phi})")
            previous_met_pt = f"{branch}[0]"
            previous_met_phi = f"{branch}[1]"
            applied_met_shift = True
        if self.tau_syst != "" and self.propagate_tes:
            # TES propagated MET (only propagate for HPS taus selected in the event)
            # no TES for boostedTaus
            print(f"MET shift : propagating TES to MET, from Tau_pt to Tau_pt{self.tau_syst}")
            branch = randomize(f"smeared_met_tau_{self.tau_syst}")
            df = df.Define(branch, "met_shifter.get_shifted_met_indices("
                           f"Tau_eta, Tau_phi, Tau_pt, Tau_mass, Tau_pt{self.tau_syst}, Tau_mass{self.tau_syst},"
                           "(!isBoostedTau && pairType_preliminary>=0) ? (pairType_preliminary == 2 ? ROOT::RVec<int>({dau1_HPSTaus_index, dau2_HPSTaus_index}) : ROOT::RVec<int>({dau2_HPSTaus_index})) : ROOT::RVec<int>({}),"
                           f"{previous_met_pt}, {previous_met_phi})")
            previous_met_pt = f"{branch}[0]"
            previous_met_phi = f"{branch}[1]"
            applied_met_shift = True
        if self.electron_syst != "" and self.propagate_electron:
            # electron scale/smearing variations propagated to MET (only for selected electron)
            print(f"MET shift : propagating electron scale/smearing to MET, from Electron_pt to Electron_pt{self.electron_syst}")
            branch = randomize(f"smeared_met_tau_{self.electron_syst}")
            df = df.Define(branch, "met_shifter.get_shifted_met_indices("
                           f"Electron_eta, Electron_phi, Electron_pt, Electron_mass, Electron_pt{self.electron_syst}, Electron_mass{self.electron_syst},"
                           "pairType_preliminary == 1 ? ROOT::RVec<int>({dau1_index}) : ROOT::RVec<int>({}),"
                           f"{previous_met_pt}, {previous_met_phi})")
            previous_met_pt = f"{branch}[0]"
            previous_met_phi = f"{branch}[1]"
            applied_met_shift = True
        
        if applied_met_shift:
            if self.met_smear_tag == "" and self.syst == "":
                raise ValueError("MET_shifter : you need to specify met_smear_tag, to give a name to the output branch")
            systs_suffix = self.systs
            if not self.propagate_jer_smearing and self.jet_syst.startswith("_smeared_"): # no propagation of JER to MET
                systs_suffix = self.systs.replace("_smeared_up", "")
                systs_suffix = systs_suffix.replace("_smeared_down", "")
            branches = [f"MET{self.met_smear_tag}_pt{systs_suffix}", f"MET{self.met_smear_tag}_phi{systs_suffix}"]
            df = df.Define(branches[0], previous_met_pt)  # pt
            df = df.Define(branches[1], previous_met_phi) # phi
        else:
            branches = []
        

        return df, branches


def metShifterRDF(**kwargs):
    """
    Module to compute MET pt and phi after applying JEC/JER/TES (configurable).
    Can propagate:
     - JEC & JER to MET (selecting Jet_pt>15 && Jet_muonIdx1<0 && Jet_muonIdx2<0 && Jet_chEmEF+Jet_neEmEF<0.9)
     - TES to MET (selected tau only)
     - electron scale and smearing to MET (sleected electron only)
    Parameters : 
     - after_jec : if True, will use as input the Jet_pt/mass_corr variables (ie with JEC reapplied, instead of Jet_pt/mass). 
                To be used in case JEC central is computed inside the framework (& not from NanoAOD)
     - propagate_jer_smearing : if False, JER smearing will not be propagated to MET (recommendation of JME for Run2)
     - propagate_tes, propagate_electron: switch to turn on/off TES & electron scale/smearing propagation
     - jet_syst : the jet systematic to use (does not have to be up or down, can be central as well)
     - tau_syst : the tau systematic to use (same)
     - met_smear_tag : affects the branch name of the output
    
    Examples : 
     - jet_syst=nom & tau_syst=corr & met_smear_tag=smeared (the default values) will compute MET_smeared_pt/phi ie the MET after JER smearing & tau energy scale central correction
     - jet_syst=smeared_up (+ defaults for others) will compute MET_smeared_pt/phi_smeared_up ie the MET after JER smearing (syst var up) & TES (central)

    Output branches : 
     - MET{self.met_smear_tag}_pt{self.systs} & MET{self.met_smear_tag}_phi{self.systs}

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: metShifterRDF
            path: Corrections.JME.met_shifter
            parameters:
                year: self.config.year
                isMC: self.dataset.process.isMC

    """
    return lambda: metShifterRDFProducer(**kwargs)





class MET_XYCorrections_RDFProducer(JetLepMetSyst):
    def _getCorrectionKey(self, var):
        return f"{var}_metphicorr_pfmet_{'mc' if self.isMC else 'data'}"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.year = kwargs.pop("year")
        self.isMC = kwargs.pop("isMC")
        self.ispreVFP = kwargs.pop("ispreVFP")

        if self.year == 2016:
            if self.ispreVFP:   self.tag = "2016preVFP_UL"
            else:               self.tag = "2016postVFP_UL"
        elif self.year == 2017: self.tag = "2017_UL"
        elif self.year == 2018: self.tag = "2018_UL"
        else:
            raise ValueError("MET_XYCorretcions : only Run2 for now")
        filename = json_path.format(self.tag)

        if not os.getenv("_corr"):
            os.environ["_corr"] = "_corr"

            if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libBaseModules.so")

            ROOT.gInterpreter.Declare(os.path.expandvars(
                '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

        
        if not os.getenv("_metXY"):
            os.environ["_metXY"] = "_metXY"

            for var in ["pt", "phi"]:
                ROOT.gInterpreter.ProcessLine(
                    f'auto corr_metXY_{var} = '
                        f'MyCorrections("{filename}", "{self._getCorrectionKey(var)}");'
                )


    def run(self, df):
        branches = []
        if self.met_syst == "_unclustered_up" or self.met_syst == "_unclustered_down":
            met_syst = ""
            # MET_tes_pt_unclustered_up does not exist yet, as we run unclustered energy after XY correction (not sure what is the recommended order)
            # so consider MET_tes_pt (nominal) in this case
            print("MET_XYCorrections: running XY corrections on non-unclusered varied MET (unclustered MET is run after XY corrections)")
        else:
            met_syst = self.met_syst
        for var in ["pt", "phi"]:
            # The MET XY corrections only go up to MET_pt = 6.5 TeV but there are MC events with MET_pt higher than that. So in that case do not correct the MET
            df = df.Define(
                f"MET{self.met_smear_tag}_xycorr_{var}{met_syst}",
                f"MET{self.met_smear_tag}_pt{met_syst} < 6500 ? corr_metXY_{var}.eval({{MET{self.met_smear_tag}_pt{met_syst}, MET{self.met_smear_tag}_phi{met_syst}, (float)PV_npvs, (float)run}}) : MET{self.met_smear_tag}_{var}{met_syst}"
            )
            branches.append(f"MET{self.met_smear_tag}_xycorr_{var}{met_syst}")
        return df, branches


def MET_XYCorrections_RDF(**kwargs):
    """
    MET XY corrections for Run2. Produces MET{smeartag}_xycorr_pt_{met_syst} & MET{smeartag}_xycorr_phi_{met_syst} branches

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: MET_XYCorrections_RDF
            path: Tools.Tools.met_xy
            parameters:
                year: self.config.year
                isMC: self.dataset.process.isMC
                runEra: self.dataset.runEra

    """
    return lambda: MET_XYCorrections_RDFProducer(**kwargs)
       

class MET_UnclusteredEnergy_RDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def run(self, df):
        branches = []

        if not self.isMC: 
            return df, []
        if self.met_syst not in ["_unclustered_down", "_unclustered_up"]:# do not do systematic variations of MET unclustered energy on top of another systematic variation propagated to MET
            print("Not computing systematic variations of MET unclustered energy on top of another systematic variation propagated to MET (met_syst=" + self.met_syst + ")")
            return df, []

        # first compute input MET px & py
        # no met_syst as input, only as output
        met_input_px =  f"MET{self.met_smear_tag}_pt * std::cos(MET{self.met_smear_tag}_phi)"
        met_input_py = f"MET{self.met_smear_tag}_pt * std::sin(MET{self.met_smear_tag}_phi)"

        # correct px and py
        met_output_px_up = met_input_px + " + MET_MetUnclustEnUpDeltaX"
        met_output_py_up = met_input_py + " + MET_MetUnclustEnUpDeltaY"
        met_output_px_down = met_input_px + " - MET_MetUnclustEnUpDeltaX"
        met_output_py_down = met_input_py + " - MET_MetUnclustEnUpDeltaY"

        # go back to pt/phi
        df = df.Define(f"MET{self.met_smear_tag}_pt_unclustered_up", f"std::sqrt(({met_output_px_up})*({met_output_px_up}) + ({met_output_py_up})*({met_output_py_up}))")
        df = df.Define(f"MET{self.met_smear_tag}_pt_unclustered_down", f"std::sqrt(({met_output_px_down})*({met_output_px_down}) + ({met_output_py_down})*({met_output_py_down}))")
        df = df.Define(f"MET{self.met_smear_tag}_phi_unclustered_up", f"std::atan2({met_output_py_up}, {met_output_px_up})")
        df = df.Define(f"MET{self.met_smear_tag}_phi_unclustered_down", f"std::atan2({met_output_py_down}, {met_output_px_down})")
        

        return df, [f"MET{self.met_smear_tag}_pt_unclustered_up", f"MET{self.met_smear_tag}_pt_unclustered_down",
            f"MET{self.met_smear_tag}_phi_unclustered_up", f"MET{self.met_smear_tag}_phi_unclustered_down"]


def MET_UnclusteredEnergy_RDF(**kwargs):
    """
    MET unclustered energy variations
    Inputs : 
     - MET{smear_tag}_pt & MET{smear_tag}_phi
    Outputs
     - MET{smear_tag}_pt_unclustered_up/down
     - MET{smear_tag}_phi_unclustered_up/down

    """
    return lambda: MET_UnclusteredEnergy_RDFProducer(**kwargs)
       



        

        