import os
import tarfile
import tempfile
import envyaml

from analysis_tools.utils import import_root
from Base.Modules.baseModules import JetLepMetSyst

ROOT = import_root()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/JME/python/smearingFiles.yaml' %
                                    os.environ['CMSSW_BASE'])

class jetSmearerRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        year = str(self.year)
        self.type = kwargs.pop("type", "")
        self.after_jec = kwargs.pop("after_jec", "")

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix + year

        if self.isMC:
            jerArchive = tarfile.open(corrCfg[self.corrKey]["fileName"], "r:gz")
            jerInputFilePath = tempfile.mkdtemp()
            jerArchive.extractall(jerInputFilePath)

            corrName = corrCfg[self.corrKey]["corrName"]
            unctName = corrCfg[self.corrKey]["unctName"]
            if self.type == "fatjerc":
                corrName.replace("AK4", "AK8")
                unctName.replace("AK4", "AK8")

            if not os.getenv("_jetSmearer"):
                os.environ["_jetSmearer"] = "_jetSmearer"
                if "/libCorrectionsJME.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gSystem.Load("libCorrectionsJME.so")
                base = "{}/{}/src/Corrections/JME".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
                ROOT.gROOT.ProcessLine(".L {}/interface/jetSmearer.h".format(base))
            if not os.getenv(f"_jetSmearer_{self.type}"):
                os.environ[f"_jetSmearer_{self.type}"] = f"_jetSmearer_{self.type}"
                ROOT.gInterpreter.Declare(
                    f'auto jet_smearer_{self.type} = jetSmearer("{jerInputFilePath}", "{corrName}", "{unctName}");'
                )

    def run(self, df):
        if not self.isMC:
            return df, []
        all_branches = df.GetColumnNames()

        # Run-2 uses fixedGridRhoFastjetAll and Run-3 Rho_fixedGridRhoFastjetAll
        rho = "fixedGridRhoFastjetAll"
        if rho not in all_branches:
            rho = "Rho_fixedGridRhoFastjetAll"

        branches = ["jet_smear_factor", "jet_smear_factor_down",
                    "jet_smear_factor_up", "jet_pt_resolution"]

        pt = "Jet_pt"
        mass = "Jet_mass"
        eta = "Jet_eta"
        phi = "Jet_phi"
        jertype = "jer"
        genjet_prefix = "GenJet"
        if self.type == "fatjerc":
            pt   = "Fat" + pt
            mass = "Fat" + mass
            eta  = "Fat" + eta
            phi  = "Fat" + phi
            jertype = "fatjer"
            genjet_prefix = "GenJetAK8"
            branches = ["fat"+b for b in branches]

        if self.after_jec:
            pt += "_corr"
            mass += "_corr"

        # we need to compute JER using as input the JEC varied jets (incl. systematics)
        # Different cases :
        # 1) nominal everything : input Jet_pt(_corr), output Jet_pt_nom, jet_syst=nom(=jet_nominal)
        # 2) JEC syst variation : input Jet_pt(_corr)_FlavorQCD_up, output Jet_pt_smeared_FlavorQCD_up, jet_syst=smeared_FlavorQCD_up
        # 3) JER syst variation : input Jet_pt(_corr), output Jet_pt_smeared_up, jet_syst=smeared_up
        # adjusting the input so the module does not try to use as input its own output
        if self.jet_syst == "_nom":
            # case 1 : nominal everything
            syst_input = self.jet_syst.replace("_nom", "", 1) # remove _nom from the input variables as this module is computing it
        elif "smeared_up" in self.jet_syst or "smeared_down" in self.jet_syst:
            # case 3 : JER syst varition (jet_syst will be "_smeared_up")
            syst_input = ""
        else:
            # case 2 : JEC syst correction (jet_syst will be "_smeared_FlavorQCD_up")
            syst_input = self.jet_syst.replace("_smeared", "", 1)
        
        pt += f"{syst_input}"
        mass += f"{syst_input}"

        print(f"Jet smearing : computing JER factor using input {pt}")

        df = df.Define(f"__{jertype}_results", f"jet_smearer_{self.type}.get_smear_vals("
            f"run, luminosityBlock, event, {pt}, {eta}, {phi}, {mass}, "
            f"{genjet_prefix}_pt, {genjet_prefix}_eta, {genjet_prefix}_phi, {genjet_prefix}_mass, {rho})")

        for ib, branch in enumerate(branches):
            df = df.Define(branch, f"ROOT::RVec<float>(__{jertype}_results[{ib}].data(), "
                f"__{jertype}_results[{ib}].size())")
        return df, branches


def jetSmearerRDF(**kwargs):
    """
    Module to compute jet smearing factors (nom, up, down) and jet_pt_resolution (for )
    Example : https://github.com/cms-nanoAOD/nanoAOD-tools/blob/d163c18096fe2c5963ff5a9764bb420b46632178/python/postprocessing/modules/jme/jetSmearer.py
    Note: UL2016_preVFP is not implemented (?????)

    Parameters : 
      - type: "" (default, for AK4) / "fatjerc" (for AK8)
      -  after_jec_rerun (default False) : if True, the input branches will be taken as Jet_pt_corr (same for eta, mass...). Meant to be used when *nominal* JEC are re-run in NanoAOD (ie we are uncorrecting nanoaod variables to reapply another JEC)
                     Not needed when JEC has already been run as part of NanoAOD production

    Output branches : 
      - (fat)jet_smear_factor (_up/_down)     
      - (fat)jet_pt_resolution
      
    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jetSmearerRDF
            path: Corrections.JME.smearing
            parameters:
                year: self.config.year
                isMC: self.dataset.process.isMC
                isUL: self.dataset.has_tag('ul')
                ispreVFP: self.config.get_aux("isPreVFP", False)
    """

    return lambda: jetSmearerRDFProducer(**kwargs)


class jetVarRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.type = kwargs.pop("type", "")
        self.after_jec = kwargs.pop("after_jec", "")

    def run(self, df):
        if not self.isMC:
            return df, []

        pt = "Jet_pt"
        mass = "Jet_mass"

        Fat = ""
        fat = ""
        if self.type == "fatjerc":
            Fat = "Fat"
            fat = "fat"
            pt   = "Fat" + pt
            mass = "Fat" + mass
        
        # we need to compute JER using as input the JEC varied jets (incl. systematics)
        # Different cases :
        # 1) nominal everything : input Jet_pt(_corr), output Jet_pt_nom, jet_syst=nom(=jet_nominal)
        # 2) JEC syst variation : input Jet_pt(_corr)_FlavorQCD_up, output Jet_pt_smeared_FlavorQCD_up, jet_syst=smeared_FlavorQCD_up
        # 3) JER syst variation : input Jet_pt(_corr), output Jet_pt_smeared_up, jet_syst=smeared_up
        if self.after_jec:
            pt += "_corr"
            mass += "_corr"
        
        # adjusting the input so the module does not try to use as input its own output
        if self.jet_syst == "_nom":
            # case 1 : nominal everything
            syst_input = self.jet_syst.replace("_nom", "", 1) # remove _nom from the input variables as this module is computing it
            variation_case = "nominal"
        elif "smeared_up" in self.jet_syst or "smeared_down" in self.jet_syst:
            # case 3 : JER syst varition (jet_syst will be "_smeared_up")
            syst_input = ""
            variation_case = "JER_variation"
        else:
            # case 2 : JEC syst correction (jet_syst will be "_smeared_FlavorQCD_up")
            syst_input = self.jet_syst.replace("_smeared", "", 1)
            variation_case = "JEC_variation"
        pt += syst_input
        mass += syst_input

        branches = []

        print(f"Jet smearing : computing new pt/mass distributions named {Fat}Jet_pt{self.jet_syst} from {pt} branch")
        if variation_case == "nominal" or variation_case == "JEC_variation":
            df = df.Define(f"{Fat}Jet_pt{self.jet_syst}", f"{pt} * {fat}jet_smear_factor")
            df = df.Define(f"{Fat}Jet_mass{self.jet_syst}", f"{mass} * {fat}jet_smear_factor")
            branches.extend([f"{Fat}Jet_pt{self.jet_syst}", f"{Fat}Jet_mass{self.jet_syst}"])
        if variation_case == "nominal":
            # computing JER uncertainties only make sense for JEC nominal (we do not add a JER variation on top of a JER variation)
            df = df.Define(f"{Fat}Jet_pt_smeared_up", f"{pt} * {fat}jet_smear_factor_up")
            df = df.Define(f"{Fat}Jet_mass_smeared_up", f"{mass} * {fat}jet_smear_factor_up")
            df = df.Define(f"{Fat}Jet_pt_smeared_down", f"{pt} * {fat}jet_smear_factor_down")
            df = df.Define(f"{Fat}Jet_mass_smeared_down", f"{mass} * {fat}jet_smear_factor_down")
            branches.extend([f"{Fat}Jet_pt_nom", f"{Fat}Jet_mass_nom",
                    f"{Fat}Jet_pt_smeared_up", f"{Fat}Jet_mass_smeared_up",
                    f"{Fat}Jet_pt_smeared_down", f"{Fat}Jet_mass_smeared_down"])
        if variation_case == "JER_variation":
            # we only compute one JER corrected collection
            if "smeared_up" in self.jet_syst:
                syst_dir = "up"
            elif "smeared_down" in self.jet_syst:
                syst_dir = "down"
            else:
                raise ValueError()
            df = df.Define(f"{Fat}Jet_pt{self.jet_syst}", f"{pt} * {fat}jet_smear_factor_{syst_dir}")
            df = df.Define(f"{Fat}Jet_mass{self.jet_syst}", f"{mass} * {fat}jet_smear_factor_{syst_dir}")
            branches.extend([f"{Fat}Jet_pt{self.jet_syst}", f"{Fat}Jet_mass{self.jet_syst}"])

        return df, branches


def jetVarRDF(**kwargs):
    """
    Module to compute jet pt and mass after applying smearing factors

    Parameters : 
      - type: "" (default, for AK4) / "fatjerc" (for AK8)
      - after_jec (default False) : if True, the input branches will be taken as Jet_pt_corr (same for eta, mass...). Meant to be used when JEC is run before smearing. 
                     Not needed when JEC has already been run as part of NanoAOD production

    Output branches : 
      - (Fat)Jet_pt_nom{self.jet_syst} & (Fat)Jet_mass_nom{self.jet_syst}
      - (Fat)Jet_pt_smeared_up/down & (Fat)Jet_mass_smeared_up/down (not computed in case of JEC variation away from nominal)

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jetVarRDF
            path: Base.Modules.smearing
            parameters:
                isMC: self.dataset.process.isMC

    """
    return lambda: jetVarRDFProducer(**kwargs)



