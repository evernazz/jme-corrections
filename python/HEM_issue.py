""" Treatment of HEM15/16 issue for 2018 UL data 
see https://cms-talk.web.cern.ch/t/question-about-hem15-16-issue-in-2018-ultra-legacy/38654?u=gagarwal
"""

import os

from Base.Modules.baseModules import JetLepMetSyst

from analysis_tools.utils import import_root, randomize

ROOT = import_root()


class hemIssueRDFProducer:
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year")
        if self.year != 2018:
            return

        if not os.getenv("_hemIssue"):
            os.environ["_hemIssue"] = "_hemIssue"
            if "/libCorrectionsJME.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libCorrectionsJME.so")
            base = "{}/{}/src/Corrections/JME".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/hemIssue.h".format(base))


    def run(self, df):
        if self.year != 2018:
            return df, []
        
        if "Jet_chEmEF" in df.GetColumnNames():
            df = df.Define("hem_jets_in_veto_result", "listJetsInHEMRegion(Jet_pt, Jet_eta, Jet_phi, Jet_jetId, Jet_puId, Jet_chEmEF, Jet_neEmEF, Jet_muonIdx1, Jet_muonIdx2)")
        else:
            print("WARNING : Jet EM energy fraction not found (Jet_chEmEF), using only JetID=tightLepVeto for HEM issue !!!")
            df = df.Define("hem_jets_in_veto_result", "listJetsInHEMRegion_hleprare_skim(Jet_pt, Jet_eta, Jet_phi, Jet_jetId, Jet_puId)")
        df = df.Define("hem_jets_in_veto", "hem_jets_in_veto_result.first")
        df = df.Define("jets_hem_preselection", "hem_jets_in_veto_result.second")
        # In MC we need to select 63.47% of events ie 6347 in 10000
        df = df.Define("hem_affected_run", "event % 10000 <= 6347" if self.isMC else "run>=319077")
        df = df.Define("hem_weight", "!(hem_affected_run && hem_jets_in_veto.size() > 0)")
        return df, ["hem_jets_in_veto", "jets_hem_preselection", "hem_affected_run", "hem_weight"]


def hemIssueRDF(**kwargs):
    """
    Deal with HEM15/16 HCAL isseu in 2018UL
    Output branches:
     - hem_jets_in_veto : list of indices into Jet collection that fall in HEM veto area (and pass preselctions)
     - jets_hem_preselection : array of bool matching Jet collection size, selecting jets that pass preselections (ie Jet id, PUid, etc) for plotting jet veto maps
     - hem_affected_run : is the event affected by HEM issue (data:affected run, MC:fraction equivalent to affected lumi in data in 2018)
     - hem_weight : false in case event should be vetoed
    """
    return lambda: hemIssueRDFProducer(**kwargs)

