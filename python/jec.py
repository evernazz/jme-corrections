import os
import tarfile
import tempfile
import itertools

from Base.Modules.baseModules import JetLepMetSyst

from analysis_tools.utils import import_root, randomize

ROOT = import_root()

class jecProviderRDFProducer(JetLepMetSyst):
    def __init__(self, isMC,
            jerInputFileName="Spring16_25nsV10_MC_PtResolution_AK4PFchs.txt",
            jec_sources=[], *args, **kwargs):

        self.isMC = isMC
        self.jet_collection = kwargs.pop("jet_collection", "Jet")
        self.pt = kwargs.pop("pt", "")
        self.eta = kwargs.pop("eta", "")
        self.mass = kwargs.pop("mass", "")

        if self.isMC:            
            if not os.getenv("JEC_PATH"):
                self.jerInputArchivePath = os.environ['CMSSW_BASE'] + \
                    "/src/Corrections/JME/data/"
                self.jerTag = jerInputFileName[:jerInputFileName.find('_MC_') + len('_MC')]
                self.jerArchive = tarfile.open(
                    self.jerInputArchivePath + self.jerTag + ".tar.gz", "r:gz")
                self.jerInputFilePath = tempfile.mkdtemp()
                self.jerArchive.extractall(self.jerInputFilePath)
                self.jerInputFileName = "RegroupedV2_%s_UncertaintySources_AK4PFchs.txt"\
                    % self.jerTag
                assert(os.path.isfile(os.path.join(self.jerInputFilePath, self.jerInputFileName)))
                os.environ["JEC_PATH"] = os.path.join(self.jerInputFilePath, self.jerInputFileName)

            if not os.getenv("_jecProvider"):
                os.environ["_jecProvider"] = "_jecProvider"
                if "/libCorrectionsJME.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gSystem.Load("libCorrectionsJME.so")
                base = "{}/{}/src/Corrections/JME".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
                try:
                    assert ROOT.jecProvider
                except AttributeError:
                    ROOT.gROOT.ProcessLine(".L {}/interface/jecProvider.h".format(base))

            self.jec_sources = jec_sources
            jec_sources_str = ", ".join(['"%s"' % jec_source for jec_source in self.jec_sources])
            self.jec_provider = randomize("jec_provider")

            ROOT.gInterpreter.Declare(
                'auto %s = jecProvider("%s", {%s});' % (
                    self.jec_provider, os.getenv("JEC_PATH"), jec_sources_str
                )
            )
        super(jecProviderRDFProducer, self).__init__(self, isMC=isMC, *args, **kwargs)

    def run(self, df):
        if not self.isMC:
            return df, []

        jecs = randomize("jec_uncertainties")

        if not self.pt:
            # NB : order here matters
            branches = [f"{name}_{d}" for (d, name) in itertools.product(
                ["up", "down"], [f"{self.jet_collection}_jec_factor_{jec_source}" for jec_source in self.jec_sources])]

            # jec_uncertainties_** is of type std::vector<std::vector<std::vector<float>>>
            # jec_uncertainties_up_** is of type std::vector<std::vector<float>>
            # use the raw Jet_pt here
            df = df.Define(jecs, f"{self.jec_provider}.get_jec_uncertainties("
                f"n{self.jet_collection}, {self.jet_collection}_pt, {self.jet_collection}_eta)").Define(
                f"{jecs}_up", f"{jecs}[0]").Define(f"{jecs}_down", f"{jecs}[1]")
            for ib, branch in enumerate(branches):
                if ib < len(self.jec_sources): # up variations
                    # {jecs}_up[{ib}] is of type std::vector<float>
                    df = df.Define(branch,
                        f"ROOT::RVec<float>({jecs}_up[{ib}].data(), {jecs}_up[{ib}].size())")
                        # f"{jecs}_up[{ib}]")
                else: # down variations
                    df = df.Define(branch,
                        f"ROOT::RVec<float>({jecs}_down[{ib - len(self.jec_sources)}].data(), "
                            f"{jecs}_down[{ib - len(self.jec_sources)}].size())")
                        # f"{jecs}_down[{ib - len(self.jec_sources)}]")
        else:
            assert self.pt and self.eta
            prefix = self.pt[:self.pt.find("pt")] # will be something like bjet1_ or fatjet_
            # jec_uncertainties_** is of type std::vector<std::vector<float>>, holding [[jec_factor_source1_up, ..., *source11_up], [.._down, ...]]
            # the pt > 0 check is to exclude the cases where the input variable is not filled (in which case it would spam logs with "input out of range"). For example in boosted category bjet2 is not filled
            df = df.Define(
                jecs, 
                f"{self.pt} > 0 ? {self.jec_provider}.get_single_jec_uncertainties({self.pt}, {self.eta}) : std::vector<std::vector<float>>()"
            ).Define(
                f"{jecs}_up", f"{self.pt} > 0 ? {jecs}[0] : std::vector<float>()" 
            ).Define(
                f"{jecs}_down", f"{self.pt} > 0 ? {jecs}[1] : std::vector<float>()"
            )

            branches = []
            for d in ["up", "down"]:
                for isource, jec_source in enumerate(self.jec_sources):
                    df = df.Define(f"{prefix}pt_{jec_source}_{d}",
                        f"{self.pt} * ({self.pt} > 0 ? {jecs}_{d}[{isource}] : 1.)")
                    branches.append(f"{prefix}pt_{jec_source}_{d}")
                    if self.mass:
                        df = df.Define(f"{prefix}mass_{jec_source}_{d}",
                            f"{self.mass} * ({self.pt} > 0 ? {jecs}_{d}[{isource}] : 1.)")
                        branches.append(f"{prefix}mass_{jec_source}_{d}")            

        return df, branches


def jecProviderRDF(**kwargs):
    """
    Module to compute jec uncertainty factors. If parameters `pt` and `eta` are included,
    the module will output the pt (and mass if it's included as a parameter) of the object
    after applying the uncertainties.
    For Run2UL, AK8 jets should use the uncertainties for AK4 (see https://cms-talk.web.cern.ch/t/jecs-for-ak8/36525)

    :param jec_sources: names of the systematic sources to consider. They depend on the year:

        - 2016: ``FlavorQCD``, ``RelativeBal``, ``HF``, ``BBEC1``, ``EC2``, ``Absolute``,\
        ``BBEC1_2016``, ``EC2_2016``, ``Absolute_2016``, ``HF_2016``, ``RelativeSample_2016``,\
        ``Total``

        - 2017: ``FlavorQCD``, ``RelativeBal``, ``HF``, ``BBEC1``, ``EC2``, ``Absolute``,\
        ``BBEC1_2017``, ``EC2_2017``, ``Absolute_2017``, ``HF_2017``, ``RelativeSample_2017``,\
        ``Total``

        - 2018: ``FlavorQCD``, ``RelativeBal``, ``HF``, ``BBEC1``, ``EC2``, ``Absolute``,\
        ``BBEC1_2018``, ``EC2_2018``, ``Absolute_2018``, ``HF_2018``, ``RelativeSample_2018``,\
        ``Total``

        Note: These are the reduced set of 11 uncertainties + the total one (to be used *instead* of the 11), for Run2

    :type jec_sources: list of str

    :param jet_collection: Name of the jet collection in input. Default: Jet (ie AK4 jets). Can be Jet or FatJet. Only used in case pt/eta/mass are not specified
    :type jet_collection: str

    :param pt: pt of the object to shift
    :type pt: str

    :param eta: pt of the object to shift
    :type eta: str

    :param mass: mass of the object to shift
    :type mass: str

    Branch outputs : 
    If pt, eta, mass are specified : 
     - {myjetprefix}_pt_{jecSource}_up/down (myjetprefix is taken from name of input pt branch : bjet1_pt_nom -> bjet1_pt_FlavorQCD_up for example)
     - ..._mass_... / ..._eta_...
    Otherwise :
     - {jet_collection}_jec_factor_{jecSource}_up/down for all jec sources (array of same size as jet_collection)

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jecProviderRDF
            path: Base.Modules.jec
            parameters:
                year: self.config.year
                isMC: self.dataset.process.isMC
                jerTag: self.config.year
                isUL: self.dataset.has_tag('ul')
                ispreVFP: self.config.get_aux('ispreVFP', False)
                jec_sources: []
                (pt: bjet1_pt_nom)
                (eta: bjet1_eta)
                (mass: bjet1_mass_nom)

    """

    isMC = kwargs.pop("isMC")
    isUL = kwargs.pop("isUL")
    year = kwargs.get("year")
    isPreVFP = kwargs.pop("ispreVFP", False)
    jetType = kwargs.pop("jetType", "AK4PFchs")
    # jerTag = kwargs.pop("jerTag", "")
    jec_sources = kwargs.pop("jec_sources", [])

    #taken from https://twiki.cern.ch/twiki/bin/view/CMS/JECDataMC
    jerTagsMC = {
        '2016': 'Summer16_07Aug2017_V11_MC',
        '2017': 'Fall17_17Nov2017_V32_MC',
        '2018': 'Autumn18_V19_MC',
        'UL2016_preVFP': 'Summer19UL16APV_V7_MC', # julia AK4CHS & AK8PUPPI
        'UL2016': 'Summer19UL16_V7_MC', # julia AK4CHS & AK8PUPPI
        'UL2017': 'Summer19UL17_V5_MC', # AK4CHS only, no AK8
        'UL2018': 'Summer19UL18_V5_MC', # both AK4CHS & AK8PUPPI
    }

    campaign_tag = "%s%s" % (("UL" if isUL else ""), year)
    if isPreVFP:
        campaign_tag += "_preVFP"

    jerTag = jerTagsMC[campaign_tag]
    jerInputFileName = jerTag + "_PtResolution_" + jetType + ".txt"

    jec_sources_per_year = {
        2016: ["FlavorQCD", "RelativeBal", "HF", "BBEC1", "EC2", "Absolute", "BBEC1_2016",
            "EC2_2016", "Absolute_2016", "HF_2016", "RelativeSample_2016", "Total"],
        2017: ["FlavorQCD", "RelativeBal", "HF", "BBEC1", "EC2", "Absolute", "BBEC1_2017",
            "EC2_2017", "Absolute_2017", "HF_2017", "RelativeSample_2017", "Total"],
        2018: ["FlavorQCD", "RelativeBal", "HF", "BBEC1", "EC2", "Absolute", "BBEC1_2018",
            "EC2_2018", "Absolute_2018", "HF_2018", "RelativeSample_2018", "Total"]
    }

    if len(jec_sources) == 0:
        jec_sources = jec_sources_per_year[int(year)]
    else:
        for jec_source in jec_sources:
            assert jec_source in jec_sources_per_year[int(year)]

    return lambda: jecProviderRDFProducer(isMC, jerInputFileName, jec_sources, **kwargs)

class jecVarRDFProducer():
    def __init__(self, isMC, jec_sources=[], jet_collection="Jet", **kwargs):
        self.isMC = isMC
        self.jec_sources = jec_sources
        self.jet_collection = jet_collection

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        for d in ["up", "down"]:
            for isource, jec_source in enumerate(self.jec_sources):
                branches.append(f"{self.jet_collection}_pt_{jec_source}_{d}")
                branches.append(f"{self.jet_collection}_mass_{jec_source}_{d}")
                df = df.Define(f"{self.jet_collection}_pt_{jec_source}_{d}", f"{self.jet_collection}_pt * {self.jet_collection}_jec_factor_{jec_source}_{d}")
                df = df.Define(f"{self.jet_collection}_mass_{jec_source}_{d}", f"{self.jet_collection}_mass * {self.jet_collection}_jec_factor_{jec_source}_{d}")

        return df, branches

def jecVarRDF(**kwargs):
    """
    Module to compute jet pt and mass after applying jec uncertainty factors. 
    
    Required RDFModules: 
    :ref:`JME_jec_jecProviderRDF`. Will use {self.jet_collection}_jec_factor_{jec_source}_up/down branches. Note that it currently uses the same jec_factor for Jet & FatJet (correct for Run2, to be checked for Run3)
    Uses  {self.jet_collection}_pt/eta (no _nom suffix, as we apply first JEC variations then JER on top)

    :param jec_sources: names of the systematic sources to consider. They depend on the year:

        - 2016: ``FlavorQCD``, ``RelativeBal``, ``HF``, ``BBEC1``, ``EC2``, ``Absolute``,\
        ``BBEC1_2016``, ``EC2_2016``, ``Absolute_2016``, ``HF_2016``, ``RelativeSample_2016``,\
        ``Total``

        - 2017: ``FlavorQCD``, ``RelativeBal``, ``HF``, ``BBEC1``, ``EC2``, ``Absolute``,\
        ``BBEC1_2017``, ``EC2_2017``, ``Absolute_2017``, ``HF_2017``, ``RelativeSample_2017``,\
        ``Total``

        - 2018: ``FlavorQCD``, ``RelativeBal``, ``HF``, ``BBEC1``, ``EC2``, ``Absolute``,\
        ``BBEC1_2018``, ``EC2_2018``, ``Absolute_2018``, ``HF_2018``, ``RelativeSample_2018``,\
        ``Total``

        Note: probably more are available, to be checked.

    :type jec_sources: list of str

    :param jet_collection: Name of the jet collection in input. Default: Jet (ie AK4 jets). Can be Jet or FatJet
    :type jet_collection: str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jecVarRDF
            path: Base.Modules.jec
            parameters:
                year: self.config.year
                isMC: self.dataset.process.isMC
                jec_sources: []

    """

    isMC = kwargs.pop("isMC")
    year = kwargs.pop("year")
    jec_sources = kwargs.pop("jec_sources", [])

    jec_sources_per_year = {
        2016: ["FlavorQCD", "RelativeBal", "HF", "BBEC1", "EC2", "Absolute", "BBEC1_2016",
            "EC2_2016", "Absolute_2016", "HF_2016", "RelativeSample_2016", "Total"],
        2017: ["FlavorQCD", "RelativeBal", "HF", "BBEC1", "EC2", "Absolute", "BBEC1_2017",
            "EC2_2017", "Absolute_2017", "HF_2017", "RelativeSample_2017", "Total"],
        2018: ["FlavorQCD", "RelativeBal", "HF", "BBEC1", "EC2", "Absolute", "BBEC1_2018",
            "EC2_2018", "Absolute_2018", "HF_2018", "RelativeSample_2018", "Total"]
    }

    if len(jec_sources) == 0:
        jec_sources = jec_sources_per_year[int(year)]
    else:
        for jec_source in jec_sources:
            assert jec_source in jec_sources_per_year[int(year)]

    return lambda: jecVarRDFProducer(isMC, jec_sources, **kwargs)

